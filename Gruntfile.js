module.exports = function(grunt) {
grunt.initConfig({

    concat_css: {
        options: {
          // Task-specific options go here. 
        },
        all: {
              src: 'assets/css/*.css',
              dest: 'assets/css/main.css'
        },
      },
       cssmin: {
            build: {
              src: 'assets/css/main.css',
              dest: 'assets/css/main.min.css'
            }
            },
            concat: {
              options: {
                separator: '\n/*next file*/\n\n'  //this will be put between conc. files
              },
              dist: {
                src: ['assets/js/jquery.min.js', 'assets/js/bootstrap.min.js', 'assets/js/jquery.nav.js', 'assets/js/fakeLoader.js', 'assets/js/jquery.datatables.min.js', 'assets/js/custom.js'],
                dest: 'assets/js/main.js'
              }
            },

            uglify: {
              build: {
                files: {
                  'assets/js/main.min.js': ['assets/js/main.js']
                }
              }
            }
  
});

grunt.registerTask('css', ['concat_css', 'cssmin']);
grunt.registerTask('js', ['concat', 'uglify']);

grunt.loadNpmTasks('grunt-contrib-uglify');
grunt.loadNpmTasks('grunt-contrib-cssmin');
grunt.loadNpmTasks('grunt-concat-css');
grunt.loadNpmTasks('grunt-contrib-concat');
};
