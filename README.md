# Assessment : Denzil Muller #

### How do I get set up? ###

* Clone repo into local or server
* Create database and upload db dump from db folder in root
* Change db config in /includes/db.php
* Run Application

### Details ###

* The assessment contains two sections : CMS and Front End
* CMS can be viewed by going to /login.php
* CMS login details 
* **Username** : Admin **Password** : R0b1nH00d

### Demo ###
[Live Demo](http://denzil.deployme.co.za/)