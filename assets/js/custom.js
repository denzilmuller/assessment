jQuery(function($) {
    "use strict";
    jQuery(document).ready(function() {

            jQuery(".fakeloader").fakeLoader({
                timeToHide: 1200,
                bgColor: "#313131",
                spinner: "spinner1"
            });

            jQuery('.confirmation').on('click', function() {
                return confirm("Are you sure you want to mark this article as deleted?");
            });


                // DataTable
                var table = jQuery('#ListingTable').DataTable();
             
                // Apply the search
                table.columns().every( function () {
                    var that = this;
             
                    jQuery( 'input', this.footer() ).on( 'keyup change', function () {
                        if ( that.search() !== this.value ) {
                            that
                                .search( this.value )
                                .draw();
                        }
                    } );
                } );

                jQuery('#ListingTable_filter input').addClass('form-control');

                 // Load more data
                jQuery('.load-more').click(function(){
                    var row = Number(jQuery('#row').val());
                    var allcount = Number(jQuery('#all').val());
                    var rowperpage = 4;
                    row = row + rowperpage;

                    if(row <= allcount){
                        jQuery("#row").val(row);

                        jQuery.ajax({
                            url: '/includes/GetData.php',
                            type: 'post',
                            data: {row:row},
                            beforeSend:function(){
                                jQuery(".load-more").text("Loading...");
                            },
                            success: function(response){

                                // Setting little delay while displaying new content
                                setTimeout(function() {
                                    // appending posts after last post with class="post"
                                    jQuery(".post:last").after(response).show().fadeIn("slow");

                                    var rowno = row + rowperpage;

                                    // checking row value is greater than allcount or not
                                    if(rowno > allcount){

                                        // Change the text and background
                                        jQuery('.load-more').text("Hide");
                                      }else{
                                        jQuery(".load-more").text("Load more");
                                    }
                                }, 500);


                            }
                        });
                    } else{
                        jQuery('.load-more').text("Loading...");

                        // Setting little delay while removing contents
                        setTimeout(function() {

                            // When row is greater than allcount then remove all class='post' element after 3 element
                            jQuery('.post:nth-child(4)').nextAll('.post').remove();

                            // Reset the value of row
                            jQuery("#row").val(0);

                            // Change the text and background
                            jQuery('.load-more').text("Load more");
                        }, 2000);


                    }

                });

           

        /*==================================
            click to collapse menu 
        ====================================*/
        jQuery(".navbar-nav li a").click(function(event) {
            // check if window is small enough so dropdown is created
            var toggle = jQuery(".navbar-toggle").is(":visible");
            if (toggle) {
                jQuery(".navbar-collapse").collapse('hide');
            }
        });

     
        /* =====================================
        	ONE PAGE NAV
        ========================================*/
        jQuery('.nav').onePageNav();
    });

    jQuery(".searchIcon").on("click", function() {
        jQuery(".searchPopup").addClass("show").find('input[type="text"]').focus();
    });

    jQuery(".closeBtn").on("click", function() {
        jQuery(this).closest(".searchPopup").removeClass("show");
    });

    jQuery(window).scroll(function() {
        if (jQuery(this).scrollTop() > 1) {
            jQuery('.navbar').addClass("sticky");
        } else {
            jQuery('.navbar').removeClass("sticky");
        }
    });
}());

jQuery(function() {
    jQuery('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = jQuery(this.hash);
            target = target.length ? target : jQuery('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                jQuery('html, body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });
});
