<?php 
require_once __DIR__ . "/lib/classes/denzil/Articles.php";
use \denzil\Articles;
include('includes/header.php');

$article = new Articles();
$slug = mysql_escape_string($_GET['slug']);
$article_title = unslugify($slug);
$article_var = $article->getArticle($article_title);
$image = ($article_var['image'] != "" ? $article_var['image'] : 'blog-1.jpg');
?>
<body>
    <div class="fakeloader"></div>
     <!-- *******************************
        HEADER
    ******************************** -->
     <?php include('includes/nav.php');?>
    <!-- *******************************
    	BLOG BANNER
    ******************************** -->
    <section class="blog-banner-wrap section-padding">
        <div class="dark-overlay">
            <div class="container">
                <h1><?php echo $article_var['title']; ?></h1>
            </div>
    </section>

    <!-- *******************************
    	BLOG SECTION
    ******************************** -->
    <section class="blog-wrap blog-detail-wrap section-padding" id="blog-detail">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="blog-box">
                                <div class="blog-image">
                                    <img src="/uploads/images/<?php echo $image; ?>" alt="Blog Name">
                                </div>
                                <div class="blog-content">
                                    <h4> <a href="" title="Blog title"><?php echo $article_var['title']; ?></a></h4>
                                    <span>- <?php echo $article_var['author']; ?></span>
                                    <p>
                                        <?php echo $article_var['excerpt']; ?>
                                    </p>
                                    <div class="blog-detail-social clearfix">
                                        <div class="team-social">
                                            <ul class="blog-social list-inline">
                                                <li><a href="#" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                                <li><a href="#" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                                                <li><a href="#" title="Google Plus"><i class="fa fa-google-plus"></i></a></li>
                                                <li><a href="#" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                                            </ul>
                                        </div>
                                        <!--/.TEAM SOCIAL SECTION -->
                                    </div>
                                </div>
                            </div>
                            <div class="blog-detail">
                                <p>
                                <?php echo $article_var['description']; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>

   <?php
include('includes/footer.php');
    ?>
