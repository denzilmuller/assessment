<?php 
require_once __DIR__ . "/lib/classes/denzil/Login.php";
require_once __DIR__ . "/lib/classes/denzil/ArticlesAdmin.php";
use \denzil\Login;
use \denzil\ArticlesAdmin;


$login = new Login();
if ($login->isUserLoggedIn() == false) {
    header("location: /login.php");
    exit;
}

$articles = new ArticlesAdmin();

?>
<?php include('includes/header.php');?>

<body>
    <!-- <div class="fakeloader"></div> -->
 <?php include('includes/nav_admin.php'); ?>
 <div class="clearfix"></div>
 <div class="container">
            <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="page-header">
            <h1><small>New Blog Listing</small></h1>
          </div>
            <a href="/cms-listing.php" class="pull-right btn btn-warning">Back to listing</a>
            <div class="clearfix"></div>

            <form method="post" action="" enctype="multipart/form-data">
            <input type="hidden" name="add" value="true" />
                  <div class="form-group">
                    <label for="Title">Title</label>
                    <input type="text" class="form-control" id="Title" name="title" required="required">
                  </div>
                  <div class="form-group">
                    <label for="Excerpt">Excerpt</label>
                    <textarea class="form-control" id="Excerpt" name="excerpt"  required="required"></textarea>
                    </div>
                     <div class="form-group">
                    <label for="Description">Description</label>
                    <textarea class="form-control" id="Description" name="description"  required="required"></textarea>
                    </div>
                  <div class="form-group">
                    <label for="image">Image</label>
                    <input type="file" id="image" name="image">
                    <p class="help-block">This will be used to display thumbnail on listing and full image on details page.</p>
                  </div>
                   <div class="form-group">
                    <label for="Author">Author</label>
                    <input type="text" class="form-control" id="Author" name="author" required="required">
                  </div>
                   <div class="form-group">
                    <label for="Active">Active</label>
                    <input type="checkbox" id="Active" name="active" checked="checked">
                  </div>
                  <button type="submit" class="btn btn-default">Submit</button>
                </form>
            </div>
            </div>
            </div>


<!-- SCRIPTS -->
<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=q4rf9szi2ot7awgamq4oa4r1p6abh0ws5h2mc6y3ok33ffl6"></script>

    <script src="assets/js/main.min.js"></script>
   <script type="text/javascript">
           tinymce.init({
            selector: '#Description',
            height: 500,
            menubar: false,
            plugins: [
              'advlist autolink lists link image charmap print preview anchor',
              'searchreplace visualblocks code fullscreen',
              'insertdatetime media table contextmenu paste code'
            ],
            toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            content_css: '//www.tinymce.com/css/codepen.min.css'
          });
    </script>
    
</body>

</html>

