<?php 
require_once __DIR__ . "/lib/classes/denzil/Login.php";
require_once __DIR__ . "/lib/classes/denzil/ArticlesAdmin.php";
use \denzil\Login;
use \denzil\ArticlesAdmin;


$login = new Login();
if ($login->isUserLoggedIn() == false) {
    header("location: /login.php");
    exit;
}

$id = mysql_escape_string($_GET['id']);

$articles = new ArticlesAdmin();
$article = $articles->deleteArticle($id);
?>
