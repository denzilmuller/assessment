<?php 
require_once __DIR__ . "/lib/classes/denzil/Login.php";
require_once __DIR__ . "/lib/classes/denzil/ArticlesAdmin.php";
use \denzil\Login;
use \denzil\ArticlesAdmin;

$login = new Login();
if ($login->isUserLoggedIn() == false) {
    header("location: /login.php");
    exit;
}
?>
<?php include('includes/header.php');?>

<body>
    <div class="fakeloader"></div>

<?php include('includes/nav_admin.php'); ?>

<div class="clearfix"></div>

 <div class="container">
            <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="page-header">
            <h1><small>Blog Listing</small></h1>
          </div>
            <a href="/cms-add.php" class="pull-right btn btn-primary">Add New Article</a>
            <div class="clearfix"></div>
            <table class="table table-striped" id="ListingTable"> 
                <thead> 
                    <tr> 
                      <th>#</th>
                      <th>Title</th> 
                      <th>Author</th> 
                      <th>Excerpt</th> 
                      <th>Active</th> 
                      <th>Update</th> 
                    </tr> 
                  </thead> 
                   <tfoot> 
                    <tr> 
                      <th>#</th>
                      <th>Title</th> 
                      <th>Author</th> 
                      <th>Excerpt</th> 
                      <th>Active</th> 
                      <th>Update</th> 
                    </tr> 
                  </thead> 
                  <tbody> 
                  <?php
                  echo ArticlesAdmin::listArticles();
                    ?>
                     </tbody> 
                     </table>

            </div>
            </div>
</div>

<!-- SCRIPTS -->
    <script src="assets/js/main.min.js"></script>

</body>

</html>

