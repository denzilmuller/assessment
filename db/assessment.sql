-- phpMyAdmin SQL Dump
-- version 4.2.0
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 17, 2017 at 05:41 PM
-- Server version: 5.6.17
-- PHP Version: 5.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `assessment`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_blog`
--

CREATE TABLE IF NOT EXISTS `tbl_blog` (
`id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `excerpt` text NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `author` varchar(255) NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `active` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tbl_blog`
--

INSERT INTO `tbl_blog` (`id`, `title`, `excerpt`, `description`, `image`, `author`, `deleted`, `active`) VALUES
(1, 'Blog Article 1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel laoreet turpis. Aliquam lobortis consequat sem. Etiam sagittis mauris non nunc hendrerit', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel laoreet turpis. Aliquam lobortis consequat sem. Etiam sagittis mauris non nunc hendreritLorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel laoreet turpis. Aliquam lobortis consequat sem. Etiam sagittis mauris non nunc hendreritLorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel laoreet turpis. Aliquam lobortis consequat sem. Etiam sagittis mauris non nunc hendreritLorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel laoreet turpis. Aliquam lobortis consequat sem. Etiam sagittis mauris non nunc hendreritLorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel laoreet turpis. Aliquam lobortis consequat sem. Etiam sagittis mauris non nunc hendreritLorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel laoreet turpis. Aliquam lobortis consequat sem. Etiam sagittis mauris non nunc hendreritLorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel laoreet turpis. Aliquam lobortis consequat sem. Etiam sagittis mauris non nunc hendreritLorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel laoreet turpis. Aliquam lobortis consequat sem. Etiam sagittis mauris non nunc hendreritLorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel laoreet turpis. Aliquam lobortis consequat sem. Etiam sagittis mauris non nunc hendreritLorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel laoreet turpis. Aliquam lobortis consequat sem. Etiam sagittis mauris non nunc hendreritLorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel laoreet turpis. Aliquam lobortis consequat sem. Etiam sagittis mauris non nunc hendrerit', 'blog-2.jpg', 'Denzil Muller', 0, 1),
(2, 'Blog Article 2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel laoreet turpis. Aliquam lobortis consequat sem. Etiam sagittis mauris non nunc hendrerit', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel laoreet turpis. Aliquam lobortis consequat sem. Etiam sagittis mauris non nunc hendreritLorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel laoreet turpis. Aliquam lobortis consequat sem. Etiam sagittis mauris non nunc hendreritLorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel laoreet turpis. Aliquam lobortis consequat sem. Etiam sagittis mauris non nunc hendreritLorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel laoreet turpis. Aliquam lobortis consequat sem. Etiam sagittis mauris non nunc hendreritLorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel laoreet turpis. Aliquam lobortis consequat sem. Etiam sagittis mauris non nunc hendreritLorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel laoreet turpis. Aliquam lobortis consequat sem. Etiam sagittis mauris non nunc hendreritLorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel laoreet turpis. Aliquam lobortis consequat sem. Etiam sagittis mauris non nunc hendrerit', 'blog-3.jpg', 'Denzil Muller', 0, 1),
(3, 'Blog Article 3', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel laoreet turpis. Aliquam lobortis consequat sem. Etiam sagittis mauris non nunc hendrerit', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel laoreet turpis. Aliquam lobortis consequat sem. Etiam sagittis mauris non nunc hendrerit, sit amet dignissim odio tincidunt. Ut vestibulum odio turpis, vitae tempor dui malesuada in. Sed maximus bibendum sem, a finibus massa accumsan sit amet. Donec imperdiet purus sit amet orci pulvinar, at ornare est accumsan. Ut sed purus ligula. Quisque commodo feugiat tellus, vel mattis dui auctor at.', '', 'Denzil Muller', 0, 1),
(4, 'Blog Article 4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel laoreet turpis. Aliquam lobortis consequat sem. Etiam sagittis mauris non nunc hendrerit', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel laoreet turpis. Aliquam lobortis consequat sem. Etiam sagittis mauris non nunc hendrerit, sit amet dignissim odio tincidunt. Ut vestibulum odio turpis, vitae tempor dui malesuada in. Sed maximus bibendum sem, a finibus massa accumsan sit amet. Donec imperdiet purus sit amet orci pulvinar, at ornare est accumsan. Ut sed purus ligula. Quisque commodo feugiat tellus, vel mattis dui auctor at.', '', 'Denzil Muller', 0, 1),
(5, 'Test Article 5', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel laoreet turpis. Aliquam lobortis consequat sem. Etiam sagittis mauris non nunc hendrerit', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel laoreet turpis. Aliquam lobortis consequat sem. Etiam sagittis mauris non nunc hendrerit, sit amet dignissim odio tincidunt. Ut vestibulum odio turpis, vitae tempor dui malesuada in. Sed maximus bibendum sem, a finibus massa accumsan sit amet. Donec imperdiet purus sit amet orci pulvinar, at ornare est accumsan. Ut sed purus ligula. Quisque commodo feugiat tellus, vel mattis dui auctor at.', '', 'Denzil Muller', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE IF NOT EXISTS `tbl_users` (
`id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `username`, `password`, `active`) VALUES
(1, 'admin', 'e85b2eb634af0a9fd3e78ad1de1faad1', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_blog`
--
ALTER TABLE `tbl_blog`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_blog`
--
ALTER TABLE `tbl_blog`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
