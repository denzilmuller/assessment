<?php
// configuration
include 'db.php';

$row = $_POST['row'];
$rowperpage = 4;

// selecting posts
$query = 'SELECT * FROM tbl_blog limit '.$row.','.$rowperpage;
$result =  $conn->query($query);

$html = '';

while ($row = $result->fetch_assoc()) {
      $title = $row['title'];
      $image = ($row['image'] != "" ? $row['image'] : 'default.jpg');
      $author = $row['author'];
      $excerpt = $row['excerpt'];
   
    // Creating HTML structure
 $html .= '<div class="col-md-3 col-sm-3 col-xs-12 post">
                            <div class="blog-box">
                                <div class="blog-image">
                                    <img src="/uploads/images/'.$image.'" alt="'.$title.'">
                                    <ul class="blog-social list-inline">
                                        <li><a href="#" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#" title="Google Plus"><i class="fa fa-google-plus"></i></a></li>
                                        <li><a href="#" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                                    </ul>
                                </div>
                                <div class="blog-content">
                                    <h4> <a href="/details/'.slugify($title).'" title="'.$title.'">'.$title.'</a></h4>
                                    <span>- '.$author.'</span>
                                    <p>
                                        '.$excerpt.'
                                    </p>
                                </div>
                            </div>
                        </div>';

}

echo $html;
?>
