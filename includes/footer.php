<!-- *******************************
      FOOTER
    ******************************** -->
    <div class="clearfix"></div>
    <footer class="section-padding" id="footer">
        <a href="#home" class="back-to-top"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
        <div class="container">
           <p>
                Copyright &copy; 2017 Denzil Muller.
            </p>
        </div>
    </footer>

    <!-- SCRIPTS COMBINED AND MINIFIED -->
    <script src="/assets/js/main.min.js"></script>
    
  </body>

</html>
