<header class="main-header" id="home">
        <div class="navbar">
            <div class="container">
                <div class="row">
                    <div class="col-md-2 col-sm-2">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="/">Denzil Muller</a>
                        </div>
                    </div>
                    <div class="col-md-10 col-sm-10">
                        <nav class="navbar-right collapse navbar-collapse">
                            <ul class="nav navbar-nav">
                            <li><a href="/login.php" class="pull-right">Admin</a></li>
                             </ul>
                            <div class="searchbar">
                                <div class="searchIcon">
                                    <i class="pe-7s-search"></i>
                                </div>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </header>
