<?php
require_once __DIR__ . "/lib/classes/denzil/Articles.php";
use \denzil\Articles;
include('includes/header.php');
?>
<body>
    <div class="fakeloader"></div>
    <!-- *******************************
    	HEADER
    ******************************** -->
    <?php include('includes/nav.php');?>

    <!-- *******************************
    	BLOG BANNER
    ******************************** -->
    <section class="blog-banner-wrap section-padding">
        <div class="dark-overlay">
            <div class="container">
                <h1>Blog</h1>
            </div>
    </section>

    <!-- *******************************
    	BLOG SECTION
    ******************************** -->
    <section class="blog-wrap section-padding" id="blog">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="row">
                    <?php 
                    $articles = new Articles();
                    $articles->displayAllArticles();
                    ?>
                    </div>
                     <a href="javascript:void();" class="btn btn-bordered blog-link load-more">load more</a>
                     <input type="hidden" id="row" value="0">
                    <input type="hidden" id="all" value="<?php echo $articles->getTotalArticles(); ?>">
                    </div>
            </div>
        </div>
    </section>

    <?php
include('includes/footer.php');
    ?>
