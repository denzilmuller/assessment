<?php
namespace denzil {

    include 'includes/db.php';
    
    /**
     * Class Articles
     * used to display the articles in front end
     */
    class Articles {

       /**
         * returns the total number of articles in the database. Used for Load More functionality.
         */
    public function getTotalArticles() {
            global $conn;
            // counting total number of posts
            $sql = "select image, title, author, excerpt from tbl_blog where deleted = 0";
            $result =  $conn->query($sql);
            return $result->num_rows ;
      }

        /**
         * displays the first 4 articles in the front end/listing page.
         */
      public function displayAllArticles() {
        global $conn;

        $sql = "select image, title, author, excerpt from tbl_blog where deleted = 0 LIMIT 4";
        $result =  $conn->query($sql);
          if ($result->num_rows > 0) {
          while ($row = $result->fetch_assoc()) {
              $title = $row['title'];
              $image = ($row['image'] != "" ? $row['image'] : 'default.jpg');
              $author = $row['author'];
              $excerpt = $row['excerpt'];
              echo ' <div class="col-md-3 col-sm-3 col-xs-12 post">
                            <div class="blog-box">
                                <div class="blog-image">
                                    <img src="/uploads/images/'.$image.'" alt="'.$title.'">
                                    <ul class="blog-social list-inline">
                                        <li><a href="#" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#" title="Google Plus"><i class="fa fa-google-plus"></i></a></li>
                                        <li><a href="#" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                                    </ul>
                                </div>
                                <div class="blog-content">
                                    <h4> <a href="/details/'.slugify($title).'" title="'.$title.'">'.$title.'</a></h4>
                                    <span>- '.$author.'</span>
                                    <p>
                                        '.$excerpt.'
                                    </p>
                                </div>
                            </div>
                        </div>';
          }
        } else {
          echo 'No blog posts loaded.';
        }
      }

        /**
         * returns an object of the article queried.
         */
        public function getArticle($title) {
          global $conn;
          $sql = "SELECT id, title, excerpt, author, image, description FROM tbl_blog WHERE title = '".$title."'";
          $result = $conn->query($sql);
          $row = $result->fetch_assoc();
          return $row;
        }

    }

  }

?>
