<?php
namespace denzil {

    /**
     * Class ArticlesAdmin
     * handles the articles process in the admin
     */
    class ArticlesAdmin
    {
  
        public function __construct()
        {

            // check the possible cms actions:
             // if user edits an article
            if (isset($_POST["edit"])) {
                $this->editArticle();
            }
             // if user deletes an article
            elseif (isset($_GET["delete"])) {
                $this->deleteArticle();
            }
             // if user adds a new article
            elseif (isset($_POST["add"])) {
                $this->addArticle();
            }

        } 

         /**
         * add article with post data
         */
        public function addArticle() {
        
        global $conn;
            
          //do insert into tbl_blog and redirect to the listing page..
          $title = mysql_escape_string($_POST['title']);
          $description = mysql_escape_string($_POST['description']);
          $excerpt = mysql_escape_string($_POST['excerpt']);
          $author = mysql_escape_string($_POST['author']);
          $active = mysql_escape_string($_POST['active']);

          $active = ($active == 'on' ? 1 : 0);

          $sql = "INSERT INTO tbl_blog (title, excerpt, description, author, active) VALUES ('".$title."', '".$excerpt."', '".$description."','".$author."','".$active."')";
          $conn->query($sql) or die($conn->error);

          $id = mysqli_insert_id($conn);

            //upload the article image
            if ($_FILES['image']['name'] != "") {
                  $target_path = "uploads/images/";
                  if (!is_dir ($target_path)) {
                    mkdir ($target_path);
                  }
                  chmod ($target_path, 0777) or die ("chmod error");
                  $target_path = $target_path . basename($_FILES['image']['name']); 
                  
                  if(move_uploaded_file($_FILES['image']['tmp_name'], $target_path) or die ("image error")) {
                    $query = "UPDATE tbl_blog SET image='".$_FILES['image']['name']."' WHERE id='".$id."'";
                   $conn->query($query) or die($conn->error);
                  } 
            }


              header("location: /cms-listing.php");

        }

          /**
         * returns object with queried article by ID
         */
        public function getArticle($id) {
          global $conn;
          $sql = "SELECT id, title, excerpt, author, image, description, active FROM tbl_blog WHERE id = ".$id;
          $result = $conn->query($sql);
          $row = $result->fetch_assoc();
          return $row;
        }

          /**
         * edit article with post data
         */
        public function editArticle() {

          global $conn;

          //do update tbl_blog and redirect to the listing page..
          $id = mysql_escape_string($_POST['id']);
          $title = mysql_escape_string($_POST['title']);
          $description = mysql_escape_string($_POST['description']);
          $excerpt = mysql_escape_string($_POST['excerpt']);
          $author = mysql_escape_string($_POST['author']);
          $active = mysql_escape_string($_POST['active']);

          $active = ($active == 'on' ? 1 : 0);

          $sql = "UPDATE tbl_blog SET title = '".$title."', excerpt = '".$excerpt."', description = '".$description."', author = '".$author."', active = '".$active."' WHERE id = ".$id;
          $conn->query($sql) or die($conn->error);

            //upload the article image
            if ($_FILES['image']['name'] != "") {
                  $target_path = "uploads/images/";
                  if (!is_dir ($target_path)) {
                    mkdir ($target_path);
                  }
                  chmod ($target_path, 0777) or die ("chmod error");
                  $target_path = $target_path . basename($_FILES['image']['name']); 
                  
                  if(move_uploaded_file($_FILES['image']['tmp_name'], $target_path) or die ("image error")) {
                    $query = "UPDATE tbl_blog SET image='".$_FILES['image']['name']."' WHERE id='".$id."'";
                   $conn->query($query) or die($conn->error);
                  } 
            }


              header("location: /cms-listing.php");
        }

          /**
         * delete article action
         */
        public function deleteArticle($id) {
         global $conn;
         $query = "UPDATE tbl_blog SET deleted=1 WHERE id='".$id."'";
          $conn->query($query) or die($conn->error);
           header("location: /cms-listing.php");
        }

          /**
         * lists all articles
         */
        public static function listArticles() {

          global $conn;
          $html = '';
           $sql = "SELECT id, title, excerpt, author, deleted, active FROM tbl_blog where deleted = 0";
          $result =  $conn->query($sql);
          if ($result->num_rows > 0) { 
                        while ($row = $result->fetch_assoc()) {

                          $active_check = ($row['active'] ? 'checked' :  '');

                $html .= '
                    <tr> 
                    <th scope="row">'.$row["id"].'</th>
                    <td><a href="/details/'.slugify($row["title"]).'" title="'.$row["title"].'" target="_blank"> '.$row["title"].'</a></td> 
                    <td>'.$row["author"].'</td> 
                    <td>'.$row["excerpt"].'</td> 
                    <td><input type="checkbox" '.$active_check.' disabled="disabled" /></td> 
                    <td><a href="/cms-edit.php?id='.$row["id"].'"><i class="fa fa-pencil-square-o"></i></a> &nbsp; &nbsp; <a href="/cms-delete.php?id='.$row["id"].'" class="confirmation"><i class="fa fa-trash-o"></i></a></td> 
                     </tr>';
                  
                  }
            } else {
              $html .=  '<p>No Articles to display.</p>';
            }      
                    
          return $html;
        }

    }
  
  }
