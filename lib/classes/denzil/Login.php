<?php
namespace denzil {

include 'includes/db.php';
    /**
     * Class login
     * handles the user's login and logout process
     */
    class Login
    {

        /**
         * @var array Collection of error messages
         */
        public $errors = array();
        /**
         * @var array Collection of success / neutral messages
         */
        public $messages = array();

        /**
         * the function "__construct()" automatically starts whenever an object of this class is created,
         * you know, when you do "$login = new Login();"
         */
        public function __construct()
        {
            // create/read session, absolutely necessary
            if (!isset($_SESSION)) {
                session_start();
            }

            // check the possible login actions:
            // if user tried to log out (happen when user clicks logout button)
            if (isset($_GET["logout"])) {
                $this->doLogout();
            }

            if (isset($_GET["log-in-first"])) {
                $this->doSignInFirstMessage();
            }

            // login via post data (if user just submitted a login form)
            elseif (isset($_POST["login"])) {
                $this->dologinWithPostData();
            }
        }

        /**
         * log in with post data
         */
        private function dologinWithPostData()
        {
            // check login form contents
            if (empty($_POST['username']) || empty($_POST['password'])) 
            {
                $this->errors[] = "Please fill in all your details.";
            } 
            else
            {

                global $conn;
                       // escape the POST stuff
                $username = mysql_escape_string($_POST['username']);
                $password = mysql_escape_string($_POST['password']);

                $password = md5($password);

                    // database query, getting all the info of the selected user (allows login via email address in the
                    // username field)

                    $sql ="select id, password from tbl_users WHERE username = '".$username."' AND password = '".$password."'";
                    $result =  $conn->query($sql);

                    // if this user exists
                    if ($result->num_rows > 0) {

                        if ($row = $result->fetch_assoc()) {

                              if ($this->verify_password($password, $row["password"]) == 0) {
                              
                                if (!isset($_SESSION)) {
                                    session_start();
                                }
       
                                // write user data into PHP SESSION (a file on your server)
                                $_SESSION['user_name'] = $username;
                                $_SESSION['user_login_status'] = 1;

                           }  
                
                        }

                    } else {
                        $this->errors[] = "This user does not exist.";
                    }
         
             }
       
        }

         /**
         * verifies the password
         */
        public function verify_password($post_password, $real_password) {
            $result = strcmp($post_password, $real_password);
            return $result;

        }

        /**
         * perform the logout
         */
        public function doLogout()
        {
            $_SESSION = array();
            if (session_id() != null)
                session_destroy();
                unset($_SESSION);
            // return a little feeedback message
            $this->messages[] = "You have been logged out.";

        }


        /**
         * display to user that they need to sign in first
         */
        public function doSignInFirstMessage()
        {
            // delete the session of the user
            $_SESSION = array();
            session_destroy();
            // return a little feeedback message
            $this->errors[] = "Please sign in first to display the dashboard.";

        }

        /**
         * simply return the current state of the user's login
         * @return boolean user's login status
         */
        public function isUserLoggedIn()
        {     
            if (isset($_SESSION['user_login_status']) AND $_SESSION['user_login_status'] == 1) {
                return true;
            }
      
            // default return
            return false;
        }
}
    
}
