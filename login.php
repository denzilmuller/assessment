<?php
require_once __DIR__ . "/lib/classes/denzil/Login.php";
use \denzil\Login;

$login = new Login();
if ($login->isUserLoggedIn() == true) {
    header("location: /cms-listing.php");
    exit;
}
?>
<?php include('includes/header.php');?>

<body>
    
 <div class="container">
            <div class="row">
            <div class="col-md-4 col-md-offset-4">
                    <div class="sign-in-form">
                     <div class="page-header">
                            <h1><small>Log In</small></h1>
          </div>

<?php
                    // show potential errors / feedback (from login object)
                    if (isset($login)) {
                        if ($login->errors) {
                            foreach ($login->errors as $error) {
                                ?>
                               <div class="alert alert-danger" role="alert">
                                    <i class="fa fa-exclamation-triangle"></i> <?php  echo $error; ?>
                                </div>
                                <?php
                            }
                        }
                        if ($login->messages) {
                            foreach ($login->messages as $message) {
                                ?>
                                <div class="alert alert-success" role="alert">
                                    <i class="fa fa-check"></i> <?php  echo $message; ?>
                                </div>
                            <?php
                            }
                        }
                    }
                    ?>

                        <form method="post" action="/login.php" id="SignInForm">

                        <div class="form-group">
                            <label>USER</label>
                            <input type="text" name="username" data-rule-required="true" data-rule-email="true" class="form-control" />
                            </div>
                             <div class="form-group">
                            <label>PASSWORD</label>
                            <input type="password" id="password" name="password" data-rule-required="true" class="form-control" />
                             </div>
                            <input type="submit" name="login" value="Log In" class="btn btn-default" />
                        </form>
                        </div>
</div>
</div>

<!-- SCRIPTS -->
    <script src="assets/js/main.min.js"></script>
  
</body>

</html>

